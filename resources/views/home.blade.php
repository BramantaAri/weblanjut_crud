<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>DATABASE MAHASISWA</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <nav class="container mt-5 bg-whtie text-dark text-center">
        <h3>DATABASE MAHASISWA</h3>
    </nav>

    <div class="container mt-4">
        <div class="row">
            <div class="col-md-10">
            </div>
            <div class="col-md-2">
                <a href="/tambah"><button type="button" class="btn btn-primary float-right">Create New</button></a>
            </div>
        </div>
    </div>

    <div class="container mt-4">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">Nama</th>
                    <th scope="col">NIM</th>
                    <th scope="col">Kelas</th>
                    <th scope="col">Prodi</th>
                    <th scope="col">Fakultas</th>
                    <th scope="col">Opsi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($mahasiswa as $m)
                <tr>
                    <td>{{$m->id}}</td>
                    <td>{{$m->nama}}</td>
                    <td>{{$m->nim}}</td>
                    <td>{{$m->kelas}}</td>
                    <td>{{$m->prodi}}</td>
                    <td>{{$m->fakultas}}</td>
                    <td width="200px">
                        <a href="/edit/{{$m->id}}"><button type="button" class="btn btn-success">Edit</button></a>
                        <a href="/hapus/{{$m->id}}"><button type="button" class="btn btn-danger">Delete</button></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
            

</body>
</html>